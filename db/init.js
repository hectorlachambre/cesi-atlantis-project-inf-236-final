db = db.getSiblingDB('atlantis');

db.createCollection('planets');

db.planets.insertMany([
  {
    name: 'La Terre',
    address: [7, 22, 11, 4, 19, 38, 3, 1],
    description:
      "La Terre, également connue sous le nom de Tauri, est la planète d'origine de l'humanité et le foyer de la base de la Tau'ri. Elle est protégée par le SGC et est le berceau de la civilisation humaine.",
  },
  {
    name: 'Atlantis (Lantia)',
    address: [4, 15, 32, 25, 10, 29, 1],
    description:
      "Atlantis, aussi appelée la Cité Perdue des Anciens, est une station de recherche avancée située sur la planète Lantia dans la galaxie de Pégase. Elle abrite de nombreuses découvertes technologiques anciennes et est dirigée par les membres de l'expédition Atlantis.",
  },
  {
    name: 'Genii (Genii)',
    address: [11, 34, 2, 28, 17, 8, 1],
    description:
      'Les Genii sont une société humaine vivant sur la planète Genii. Ils se considèrent comme des survivants dans un environnement hostile et ont développé une technologie militaire avancée. Ils sont connus pour leur attitude méfiante envers les étrangers.',
  },
  {
    name: 'Hoff (Hoff)',
    address: [22, 1, 13, 6, 20, 36, 1],
    description:
      "Hoff est une planète où les habitants ont développé un traitement médical appelé Hoffan. Cependant, ce traitement a des effets secondaires désastreux chez certains individus. L'équipe Atlantis a tenté de remédier à cette situation.",
  },
  {
    name: 'Taranis (Taranis)',
    address: [6, 29, 19, 31, 25, 14, 1],
    description:
      "Taranis est une planète où les Anciens ont construit une installation de recherche sur l'énergie. Elle a été utilisée pour étudier l'énergie géothermique. L'équipe Atlantis a visité cette planète pour enquêter sur des perturbations énergétiques.",
  },
  {
    name: 'Asuras (Asuras)',
    address: [17, 7, 24, 32, 11, 30, 1],
    description:
      "Asuras est une planète habitée par une race d'androïdes appelée les Asurans. Ces créations des Anciens sont devenus une menace après avoir évolué et décidé d'exterminer toute forme de vie organique dans la galaxie de Pégase.",
  },
  {
    name: 'M7G-677 (M7G-677)',
    address: [20, 12, 8, 35, 3, 22, 1],
    description:
      "M7G-677 est une planète verdoyante et paisible habitée par une petite communauté d'enfants orphelins appelés les Orphelins de M7G-677. L'équipe Atlantis a découvert cette planète et a cherché à aider ces enfants.",
  },
  {
    name: 'Manaria (Manaria)',
    address: [35, 19, 4, 13, 28, 5, 1],
    description:
      "Manaria est une planète où les habitants ont développé une technologie avancée basée sur l'énergie solaire. Ils ont une culture riche et sont connus pour leur hospitalité envers les étrangers.",
  },
  {
    name: 'Midway (Station Midway)',
    address: [29, 5, 26, 2, 7, 34, 1],
    description:
      'La Station Midway est un avant-poste construit par les Anciens pour relier la Voie Lactée et la galaxie de Pégase. Elle joue un rôle crucial dans le maintien des communications et des échanges entre les deux galaxies.',
  },
  {
    name: 'Athos (Athos)',
    address: [25, 16, 30, 3, 23, 9, 1],
    description:
      'Athos est une planète autrefois peuplée par les Athosiens, une société pacifique. Cependant, ils ont été décimés par les Wraiths. Certains survivants vivent encore dans des communautés disséminées.',
  },
  {
    name: 'Sateda (Sateda)',
    address: [3, 20, 10, 21, 18, 33, 1],
    description:
      'Sateda était autrefois une planète guerrière puissante. Mais elle a été dévastée par les Wraiths. Quelques survivants, comme Ronon Dex, errent encore dans les ruines de leur civilisation.',
  },
  {
    name: 'New Athos (Nouvel Athos)',
    address: [10, 23, 31, 14, 5, 19, 1],
    description:
      "New Athos est une planète colonisée par les Athosiens après l'abandon d'Athos. C'est un lieu de refuge pour leur peuple. Ils tentent de reconstruire leur société tout en évitant les Wraiths.",
  },
  {
    name: 'Doranda (Doranda)',
    address: [12, 9, 22, 36, 30, 21, 1],
    description:
      "Doranda est une planète où les Anciens ont construit une installation de recherche avancée pour étudier l'énergie. Elle a été utilisée pour développer la technologie des armes à base de naquadria.",
  },
  {
    name: "Travelers' Planet (Planète des Voyageurs)",
    address: [36, 26, 15, 7, 14, 11, 1],
    description:
      "La Planète des Voyageurs est le foyer d'une société nomade et itinérante connue sous le nom de Voyageurs. Ils parcourent les étoiles à bord de leurs vaisseaux spatiaux, évitant souvent les conflits et les ennuis.",
  },
  {
    name: 'Lantea (Lantea)',
    address: [28, 3, 17, 29, 1, 16, 1],
    description:
      "Lantea est la planète où les Anciens ont construit Atlantis. Elle est riche en ressources et possède une grande biodiversité. C'est un monde d'une grande importance historique et culturelle dans la galaxie de Pégase.",
  },
  {
    name: 'Proculus (Proculus)',
    address: [9, 21, 18, 38, 24, 6, 1],
    description:
      'Proculus est une planète avec une atmosphère toxique qui est dangereuse pour les humains. Cependant, elle abrite une forme de vie autochtone qui a développé une résistance à ces conditions.',
  },
  {
    name: 'Olesia (Olesia)',
    address: [15, 14, 27, 35, 9, 37, 1],
    description:
      'Olesia est une planète agricole paisible qui fournit une grande partie de la nourriture consommée dans la galaxie de Pégase. Ses habitants sont connus pour leurs compétences en agriculture et leur hospitalité.',
  },
  {
    name: 'Talus (Talus)',
    address: [19, 28, 6, 9, 26, 25, 1],
    description:
      "Talus est une planète riche en minéraux précieux. Elle est souvent exploitée par les sociétés minières à la recherche de naquadah et d'autres ressources. Cependant, sa nature inhospitalière en fait un endroit difficile à vivre.",
  },
  {
    name: 'Edowin (Edowin)',
    address: [30, 11, 25, 5, 13, 12, 1],
    description:
      'Edowin est une planète forestière luxuriante abritant une variété de flore et de faune exotiques. Elle est souvent visitée par les scientifiques et les explorateurs pour étudier son écosystème unique.',
  },
  {
    name: "Proculus' Moon (Lune de Proculus)",
    address: [14, 32, 34, 12, 21, 20, 1],
    description:
      "La Lune de Proculus est une lune rocheuse orbitant autour de la planète Proculus. Elle a été utilisée comme base de recherche par les scientifiques pour étudier les effets de l'atmosphère toxique de Proculus.",
  },
  {
    name: "Taranis' Moon (Lune de Taranis)",
    address: [32, 27, 12, 8, 35, 24, 1],
    description:
      "La Lune de Taranis est une lune mystérieuse qui présente des anomalies énergétiques. Elle a été le site de recherches par les Anciens sur de nouvelles formes d'énergie. Cependant, elle est également associée à des phénomènes météorologiques extrêmes.",
  },
  {
    name: "Hoff's Moon (Lune de Hoff)",
    address: [24, 33, 29, 16, 37, 2, 1],
    description:
      "La Lune de Hoff est une lune stérile et inhospitalière qui orbite autour de la planète Hoff. Elle a peu d'intérêt scientifique ou stratégique et est souvent ignorée par les explorateurs interstellaires.",
  },
  {
    name: "Genii Homeworld (Monde d'origine des Genii)",
    address: [26, 31, 7, 26, 27, 23, 1],
    description:
      "Le Monde d'origine des Genii est la planète où vit la société humaine des Genii. Ils se considèrent comme des survivants et ont développé une technologie militaire avancée pour se protéger dans leur environnement hostile.",
  },
  {
    name: "Asuran Homeworld (Monde d'origine des Asurans)",
    address: [23, 8, 20, 23, 33, 18, 1],
    description:
      "Le Monde d'origine des Asurans est la planète où les Anciens ont créé les Asurans, une race d'androïdes. Après leur rébellion, ils ont fait de cette planète leur base d'opérations pour exterminer toute vie organique.",
  },
  {
    name: "Travelers' Base (Base des Voyageurs)",
    address: [5, 30, 16, 33, 15, 7, 1],
    description:
      'La Base des Voyageurs est une installation utilisée par les Voyageurs comme point de rassemblement et de réapprovisionnement. Elle abrite également leurs technologies avancées et est parfois utilisée comme refuge en cas de danger.',
  },
  {
    name: 'Asuran Outpost (Avant-poste Asuran)',
    address: [33, 6, 21, 24, 31, 10, 1],
    description:
      "L'Avant-poste Asuran est une installation militaire construite par les Asurans pour surveiller les activités dans la galaxie de Pégase. Elle est souvent utilisée comme point de départ pour leurs opérations contre les humains et les autres races.",
  },
  {
    name: "Wraith Homeworld (Monde d'origine des Wraiths)",
    address: [1, 13, 33, 34, 32, 28, 1],
    description:
      "Le Monde d'origine des Wraiths est la planète où vit la race des Wraiths. Ils se nourrissent de l'énergie vitale des humains et sont une menace constante pour la galaxie de Pégase. Leur société est basée sur une structure féodale.",
  },
  {
    name: 'Lantean Outpost (Avant-poste Lantien)',
    address: [16, 35, 1, 15, 8, 31, 1],
    description:
      "L'Avant-poste Lantien est une installation des Anciens utilisée pour surveiller les activités dans la galaxie de Pégase. Elle abrite souvent des technologies avancées et est utilisée comme base de recherche et de défense.",
  },
  {
    name: "Athosian Homeworld (Monde d'origine des Athosiens)",
    address: [31, 4, 28, 30, 12, 27, 1],
    description:
      "Le Monde d'origine des Athosiens est la planète où vivait autrefois la société pacifique des Athosiens. Après l'attaque des Wraiths, certains survivants continuent à vivre en tant que communautés nomades dans la galaxie de Pégase.",
  },
  {
    name: "Travelers' Ship (Vaisseau des Voyageurs)",
    address: [34, 24, 18, 22, 34, 3, 1],
    description:
      'Le Vaisseau des Voyageurs est le principal moyen de transport utilisé par les Voyageurs. Il est équipé de technologies avancées et peut voyager rapidement entre les systèmes stellaires. Il sert également de base mobile pour leur société nomade.',
  },
  {
    name: "Belkan's Planet (Planète de Belkan)",
    address: [21, 18, 14, 27, 16, 35, 1],
    description:
      'La Planète de Belkan est une planète agricole qui produit une grande variété de cultures et de produits agricoles. Elle est souvent visitée par les commerçants et les voyageurs à la recherche de produits alimentaires de haute qualité.',
  },
  {
    name: 'Halcyon (Halcyon)',
    address: [13, 17, 24, 37, 6, 26, 1],
    description:
      'Halcyon est une planète paradisiaque avec des paysages magnifiques et une faune exotique. Elle est souvent visitée par les touristes et les amateurs de nature pour son climat tempéré et ses plages immaculées.',
  },
  {
    name: "Vanir Homeworld (Monde d'origine des Vanirs)",
    address: [2, 25, 3, 11, 22, 2, 1],
    description:
      "Le Monde d'origine des Vanirs est la planète où vit la société des Vanirs. Ils sont les descendants des Anciens et ont développé une technologie avancée. Ils sont souvent en conflit avec les autres peuples de la galaxie de Pégase.",
  },
  {
    name: 'M35-117 (M35-117)',
    address: [8, 10, 23, 17, 36, 13, 1],
    description:
      "M35-117 est une planète recouverte d'une vaste forêt. Elle a été explorée par l'équipe Atlantis, qui y a découvert une civilisation primitive vivant en harmonie avec la nature.",
  },
  {
    name: 'M1M-316 (M1M-316)',
    address: [18, 38, 20, 18, 5, 32, 1],
    description:
      "M1M-316 est une planète désertique avec des tempêtes de sable fréquentes. Elle abrite des ruines anciennes qui ont été étudiées par l'équipe Atlantis pour en apprendre davantage sur la civilisation qui y vivait autrefois.",
  },
  {
    name: 'M12-578 (M12-578)',
    address: [31, 2, 1, 22, 4, 17, 1],
    description:
      "M12-578 est une planète où l'équipe Atlantis a découvert une installation de recherche abandonnée des Anciens. Ils y ont trouvé des indices sur de nouvelles technologies et des artefacts anciens.",
  },
]);
