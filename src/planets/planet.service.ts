import { Injectable } from '@nestjs/common';
import { Planet } from './planet.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class PlanetService {
  constructor(@InjectModel(Planet.name) private planetModel: Model<Planet>) {}

  async findAllPlanets(): Promise<Planet[]> {
    return this.planetModel.find().exec();
  }
}
