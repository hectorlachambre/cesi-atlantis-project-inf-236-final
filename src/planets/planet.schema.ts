import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { HydratedDocument } from 'mongoose';

export type PlanetDocument = HydratedDocument<Planet>;

@Schema({ collection: 'planets' })
export class Planet {
  @ApiProperty()
  @Prop()
  name: string;

  @ApiProperty()
  @Prop()
  description: string;

  @ApiProperty()
  @Prop()
  address: Array<number>;
}

export const PlanetSchema = SchemaFactory.createForClass(Planet);
